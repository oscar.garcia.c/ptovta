<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct() {
		parent::__construct();
		// var_dump($this->session->userdata);

		// Se valida si el usuario esta logeado
		if( !$this->session->userLogged ){
			if( $this->router->fetch_class() != "login" ){
				redirect('/login', 'refresh');
			}
		}

		// Se fija el template por defecto para las vistas
		$this->template
				->set_layout("dashboard")
				->set_partial("leftMenuAdmin", "partials/leftMenuAdmin")
				->set_partial("topNav", "partials/topNav")
				->set_partial("footer", "partials/footer");

		$this->data = array();
		if( $this->session->userLogged ) {
			$this->data["userLogged"]	= $this->session->userLogged;
			$this->data["userName"]		= $this->session->userName;
			$this->data["userTipo"]		= $this->session->userTipo;
			$this->data["userTipoId"]		= $this->session->userTipoId;
		}
	}
}