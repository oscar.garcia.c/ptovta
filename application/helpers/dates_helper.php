<?php
if (!function_exists('dateRange')) {
	/**
	 * [dateRange description]
	 * @param  [type] $first         [description]
	 * @param  [type] $last          [description]
	 * @param  string $step          [description]
	 * @param  string $output_format [description]
	 * @return [type]                [description]
	 */
	function dateRange( $first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {
		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);

		while( $current <= $last ) {
			$dates[] = date($output_format, $current);
			$current = strtotime($step, $current);
		}

		return $dates;
	}
}
if (!function_exists('getStartAndEndDate')) {
	/**
	 * [getStartAndEndDate description]
	 * @author Jose Vera D. <jose.vera@consultor.wom.cl>
	 * @version 1.0	Version Base
	 * @param   [type]      $week [description]
	 * @param   [type]      $year [description]
	 * @return  [type]            [description]
	 */
	function getStartAndEndDate( $week, $year ) {
		$dto = new DateTime();
		$dto->setISODate($year, $week);
		$ret['week_start'] = $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$ret['week_end'] = $dto->format('Y-m-d');
		return $ret;
	}
}
?>