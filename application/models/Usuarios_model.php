<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuarios_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function checkLogin($rut, $pass){
		$aux = $this->db
						->select("usu.nombre, tusu.nombre as tipoUsuarioNombre, tusu.idtipousuario as tipoUsuarioID")
						->from("usuarios usu")
						->join("tipousuarios tusu","usu.tipousuarios_idtipousuario = tusu.idtipousuario")
						->where("usu.rut",$rut)
						->where("usu.password",$pass)
						->where("usu.activo",1)
						->get()->result();

		$ret = array();
		if (count($aux)==1) {
			$ret["ok"] = true;
			$ret["data"] = $aux[0];
		}else{
			$ret["ok"] = false;
		}

		return $ret;
	}



























//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function now(){
		return $this->db->select("now() as date")->get()->result();
	}

	/**
	 * [checkPermisosSistema description]
	 * @param  [type] $idUser [description]
	 * @param  [type] $system [description]
	 * @return [type]         [description]
	 */
	function checkPermisosSistema( $idUser, $system = null ) {
		$return = array();
		if( !is_null( $system ) )
			$this->db->where("syst.idwom_sistema", $system);
		$aux = $this->db
					->select("user.idwom_usuario")
					->select("user.nombre AS nombreUsuario")
					->select("user.email AS emailUsuario")
					->select("user.wom_area_idwom_area")
					->select("area.nombre AS area")
					->select("rlss.idwom_rol")
					->select("rlss.nombre AS nombreRol")
					->select("syst.idwom_sistema")
					->select("syst.nombre AS nombreSistema")
					->select("syst.descripcion AS descSistema")
					->select("syst.url AS urlSistema")
					->select("syst.prefijo AS prefixSistema")
					->from("wom_usuario user")
					->join("wom_usuariorol rlus", "rlus.wom_usuario_idwom_usuario = user.idwom_usuario")
					->join("wom_rol rlss", "rlss.idwom_rol = rlus.wom_rol_idwom_rol")
					->join("wom_sistema syst", "syst.idwom_sistema = rlss.wom_sistema_idwom_sistema")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area")
					->where("user.activo", 1)
					->where("user.idwom_usuario", $idUser )
					->where("syst.activo", 1)
					->get()->result();

		if( count( $aux ) > 0 ) {
			$idActual = null;
			$usuario = array();
			for( $i=0 ; $i<count($aux) ; $i++ ) {
				if( is_null( $idActual ) ) {
					$idActual = $aux[$i]->idwom_usuario;
					$usuario = array(
						"idwom_usuario"       => $aux[$i]->idwom_usuario,
						"nombreUsuario"       => $aux[$i]->nombreUsuario,
						"emailUsuario"        => $aux[$i]->emailUsuario,
						"wom_area_idwom_area" => $aux[$i]->wom_area_idwom_area,
						"area"                => $aux[$i]->area,
						"sistemas"            => array()
					);
				}
				array_push( $usuario["sistemas"], arrayToObject( array(
					"idwom_sistema" => $aux[$i]->idwom_sistema,
					"nombreSistema" => $aux[$i]->nombreSistema,
					"descSistema"   => $aux[$i]->descSistema,
					"urlSistema"    => $aux[$i]->urlSistema,
					"prefixSistema" => $aux[$i]->prefixSistema,
					"idwom_rol"     => $aux[$i]->idwom_rol,
					"nombreRol"     => $aux[$i]->nombreRol
				) ) );
			}
			// respaldo de variable
			$aux = $usuario["sistemas"];
			unset( $usuario["sistemas"] );
			$usuario = arrayToObject( $usuario );
			$usuario->sistemas = $aux;

			if( !empty( $usuario ) ) {
				$return["ok"]   = true;
				$return["data"] = $usuario;
			} else {
				$return["ok"]    = false;
				$return["error"] = "Credenciales no corresponden";
			}
		} else
			$return = array(
				"ok"    => false,
				"error" => "Credenciales no corresponden"
			);

		return $return;
	}


	/**
	 * [doSession description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	function doSession( $data ) {
		if( count( $data->sistemas ) > 0 ) {
			// Se consigue el listado de permisos que tiene el rol del usuario
			$prm = $this->db
						->select("prms.idwom_permiso AS id", false)
						->select("rlus.nombre AS rol", false)
						->select("prms.nombre AS permiso", false)
						->from("wom_rol rlus")
						->join("wom_rolpermiso rlpm", "rlpm.wom_rol_idwom_rol = rlus.idwom_rol")
						->join("wom_permiso prms", "prms.idwom_permiso = rlpm.wom_permiso_idwom_permiso")
						->where("rlus.idwom_rol", $data->sistemas[0]->idwom_rol)
						->get()->result();

			$xts = $this->db
						->select("prms.idwom_permiso AS id", false)
						->select("prms.nombre AS permiso", false)
						->select("merg.denegate AS denegar", false)
						->from("wom_usuariopermiso merg")
						->join("wom_permiso prms", "prms.idwom_permiso = merg.wom_permiso_idwom_permiso")
						->where("merg.wom_usuario_idwom_usuario", $data->idwom_usuario)
						->get()->result();
			$ros = $this->db
						->select("wom_rol_idwom_rol")
						->from("wom_usuariorol rol")
						->where("rol.wom_usuario_idwom_usuario", $data->idwom_usuario)
						->get()->result();

			$sis = $this->db
						->select("wom_sistema_idwom_system")
						->from("wom_usuariosistema sis")
						->where("sis.wom_usuario_idwomuser", $data->idwom_usuario)
						->get()->result();
			$empr = $this->db
                        ->select("wom_empresa.*, wom_empresausuario.idwom_empresausuario, wom_empresausuario.interno")
                        ->from("womempresas.wom_empresa")
                        ->join("womempresas.wom_empresausuario", "wom_empresausuario.wom_empresa_idwom_empresa = wom_empresa.idwom_empresa")
                        ->where("wom_empresausuario.wom_usuario_idwom_usuario", $data->idwom_usuario)
                        ->get()->result();

			$sistemas = array();
			$roles = array();
			foreach ($sis as $key => $value) {
				array_push($sistemas, $value->wom_sistema_idwom_system);
			}
			foreach ($ros as $key => $value) {
				array_push($roles, $value->wom_rol_idwom_rol);
			}
			// Se valida si realmente tenia permisos el rol
			if( count( $prm ) > 0 ) {
				// Se crea un arreglo con los permisos ordenado
				$permisos = array();
				foreach( $prm as $value )
					$permisos[$value->id] = $value->permiso;
				// Añado los permisos que tiene por usuario
				foreach( $xts as $value )
					if( intval( $value->denegar ) == 0 )
						$permisos[$value->id] = $value->permiso;
				// Denego los permisos que tiene denegados por usuario
				foreach( $xts as $value )
					if( intval( $value->denegar ) == 1 )
						unset( $permisos[$value->id] );

				// Se calcula el prefijo del sistema al que se esta intentando ingresar
				$prefix = $data->sistemas[0]->prefixSistema;
				// Se crea arreglo para la session del usuario
				$sess = array(
					"womLogged"		   => true,
					"womUserID"		   => $data->idwom_usuario,
					"sistemas"		   => $sistemas,
					"roles"		  	   => $roles,
					"empresa"          => (count($empr) > 0) ? $empr[0] : null,
					$prefix."Logged"   => true,
					$prefix."ID"       => $data->idwom_usuario,
					$prefix."Name"     => $data->nombreUsuario,
					$prefix."Email"    => $data->emailUsuario,
					$prefix."IDArea"   => $data->wom_area_idwom_area,
					$prefix."Area"     => $data->area,
					$prefix."Rol"      => $data->sistemas[0]->idwom_rol,
					$prefix."Permisos" => $permisos
				);
				$return = array(
					"ok"   => true,
					"data" => $sess
				);
			} else
				$return = array(
					"ok"    => false,
					"error" => "El rol del usuario no tiene permisos sobre el sistema"
				);
		} else
			$return = array(
				"ok"    => false,
				"error" => "El usuario no tiene permisos configurados para ingresar en el sistema"
			);

		return $return;
	}
	function getUserByToken( $token ) {
		$aux = $this->db
					->select("user.idwom_usuario AS id")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS idarea")
					->select("area.nombre AS area")
					->select("user.activo")
					->select("user.reset")
					->select("user.receiveMail")
					->select("urol.idwom_rol AS idrol")
					->select("urol.nombre AS rol")
					->select("syst.idwom_sistema AS idsistema")
					->select("syst.nombre AS sistema")
					->select("syst.descripcion AS sistDesc")
					->select("syst.url AS sistUrl")
					->select("syst.urlsample AS sistUrlSample")
					->select("syst.activo AS sistActivo")
					->select("syst.prefijo AS sistPrefijo")
					->select("syst.screenshot AS sistScreenshot")
					->select("syst.icon AS sistIcon")
					->select("if (mail_last_check <= DATE_ADD(NOW(), INTERVAL -1 MONTH) || mail_last_check is null, '1', '0') as mail_last_check")
					->from("wom_usuario user")
					->join("wom_reset_tokens tokens", "tokens.wom_usuario_idwom_usuario = user.idwom_usuario")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area", "left")
					->join("wom_usuariorol mgrl", "mgrl.wom_usuario_idwom_usuario = user.idwom_usuario", "left")
					->join("wom_rol urol", "urol.idwom_rol = mgrl.wom_rol_idwom_rol", "left")
					->join("wom_sistema syst", "syst.idwom_sistema = urol.wom_sistema_idwom_sistema", "left")
					->where("tokens.token", $token)
					->where("tokens.activo", 1)
					->where("tokens.valid_until > now()")
					->group_by("idwom_usuario")
					->get()->result();
		return $aux;
	}

	function getUserByMail( $email ) {
		$aux = $this->db
					->select("user.idwom_usuario AS id")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS idarea")
					->select("area.nombre AS area")
					->select("user.activo")
					->select("user.reset")
					->select("user.receiveMail")
					->select("urol.idwom_rol AS idrol")
					->select("urol.nombre AS rol")
					->select("syst.idwom_sistema AS idsistema")
					->select("syst.nombre AS sistema")
					->select("syst.descripcion AS sistDesc")
					->select("syst.url AS sistUrl")
					->select("syst.urlsample AS sistUrlSample")
					->select("syst.activo AS sistActivo")
					->select("syst.prefijo AS sistPrefijo")
					->select("syst.screenshot AS sistScreenshot")
					->select("syst.icon AS sistIcon")
					->select("if (mail_last_check <= DATE_ADD(NOW(), INTERVAL -1 MONTH) || mail_last_check is null, '1', '0') as mail_last_check")
					->from("wom_usuario user")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area", "left")
					->join("wom_usuariorol mgrl", "mgrl.wom_usuario_idwom_usuario = user.idwom_usuario", "left")
					->join("wom_rol urol", "urol.idwom_rol = mgrl.wom_rol_idwom_rol", "left")
					->join("wom_sistema syst", "syst.idwom_sistema = urol.wom_sistema_idwom_sistema", "left")
					->where("user.email", $email)
					->get()->result();
		return $aux;
	}

	function getReset1Minute($id){
		$aux =	$this->db
					->from("wom_reset_tokens tk")
					->where("wom_usuario_idwom_usuario", $id)
					->where("timestamp >= DATE_SUB(now(),  interval 30 second)")
					->get()->result();
		if (count($aux) > 2){
			return true;
		}else{
			return false;
		}
	}

	function getContractByMail($email, $rol){
		$aux = $this->db
					->select("empusu.idwom_empresausuario id, emp.idwom_empresa")
					->from("wom_contratista cont")
					->join("wom_contratistarol control", "control.wom_contratista_idwom_contratista = cont.idwom_contratista")
					->join($this->wom_empresas.".wom_empresausuario empusu", "empusu.wom_contratista_idwom_contratista = cont.idwom_contratista")
					->join($this->wom_empresas.".wom_empresa emp", "empusu.wom_empresa_idwom_empresa = emp.idwom_empresa")
					->where("wom_rol_idwom_rol", $rol)
					->where("cont.email", $email)
					->get()->result();
		return $aux;

	}

	/**
	 * [getUserByID description]
	 * @param  [type] $iduser [description]
	 * @return [type]         [description]
	 */
	function getUserByID( $iduser ) {
		$aux = $this->db
					->select("user.idwom_usuario AS id")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS idarea")
					->select("area.nombre AS area")
					->select("user.activo")
					->select("urol.idwom_rol AS idrol")
					->select("urol.nombre AS rol")
					->select("syst.idwom_sistema AS idsistema")
					->select("syst.nombre AS sistema")
					->select("syst.descripcion AS sistDesc")
					->select("syst.url AS sistUrl")
					->select("syst.urlsample AS sistUrlSample")
					->select("syst.activo AS sistActivo")
					->select("syst.prefijo AS sistPrefijo")
					->select("syst.screenshot AS sistScreenshot")
					->select("syst.icon AS sistIcon")
					->from("wom_usuario user")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area")
					->join("wom_usuariorol mgrl", "mgrl.wom_usuario_idwom_usuario = user.idwom_usuario", "left")
					->join("wom_rol urol", "urol.idwom_rol = mgrl.wom_rol_idwom_rol", "left")
					->join("wom_sistema syst", "syst.idwom_sistema = urol.wom_sistema_idwom_sistema", "left")
					->where("user.idwom_usuario", $iduser)
					->where("syst.privado", 0)
					->get()->result();

		if( count( $aux ) > 0 ) {
			$sistemas = array();
			$user = array(
				"id"       => $aux[0]->id,
				"nombre"   => $aux[0]->nombre,
				"email"    => $aux[0]->email,
				"telefono" => $aux[0]->telefono,
				"idarea"   => $aux[0]->idarea,
				"area"     => $aux[0]->area,
				"activo"   => $aux[0]->activo,
				"sistemas" => array()
			);
			foreach( $aux as $value ) {
				array_push( $sistemas, array(
					"idsistema"   => $value->idsistema,
					"sistema"     => $value->sistema,
					"sistDesc"    => $value->sistDesc,
					"sistUrl"     => $value->sistUrl,
					"sistUrlSample" => $value->sistUrlSample,
					"sistActivo"  => $value->sistActivo,
					"sistPrefijo" => $value->sistPrefijo,
					"sistScreen"  => (is_null($value->sistScreenshot))?null:base_url().$value->sistScreenshot,
					"sistIcon"    => $value->sistIcon,
					"idrol"       => $value->idrol,
					"rol"         => $value->rol,
				) );
			}

			$user["sistemas"] = $sistemas;
		} else
			$user = array();

		return $user;
	}

	/**
	 * [getUserByArr description]
	 * @param  [type] $arr [description]
	 * @return [type]      [description]
	 */
	function getUserByArr( $arr ) {
		$aux = $this->db
					->select("user.idwom_usuario AS id")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS idarea")
					->select("area.nombre AS area")
					->select("user.activo")
					->select("urol.idwom_rol AS idrol")
					->select("urol.nombre AS rol")
					->select("syst.idwom_sistema AS idsistema")
					->select("syst.nombre AS sistema")
					->select("syst.descripcion AS sistDesc")
					->select("syst.url AS sistUrl")
					->select("syst.urlsample AS sistUrlSample")
					->select("syst.activo AS sistActivo")
					->select("syst.prefijo AS sistPrefijo")
					->select("syst.screenshot AS sistScreenshot")
					->from("wom_usuario user")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area")
					->join("wom_usuariorol mgrl", "mgrl.wom_usuario_idwom_usuario = user.idwom_usuario", "left")
					->join("wom_rol urol", "urol.idwom_rol = mgrl.wom_rol_idwom_rol", "left")
					->join("wom_sistema syst", "syst.idwom_sistema = urol.wom_sistema_idwom_sistema", "left")
					->where_in("user.idwom_usuario", $arr)
					->where("syst.privado", 0)
					->order_by("user.idwom_usuario")
					->get()->result();

		if( count( $aux ) > 0 ) {
			$actual = 0;
			$cont = -1;
			$users = array();
			foreach( $aux as $key => $value ) {
				if( $actual == 0 || $value->id != $actual ) {
					$actual = $value->id;
					array_push( $users, array(
						"id"       => $value->id,
						"nombre"   => $value->nombre,
						"email"    => $value->email,
						"telefono" => $value->telefono,
						"idarea"   => $value->idarea,
						"area"     => $value->area,
						"activo"   => $value->activo,
						"sistemas" => array()
					) );
					$cont++;
				}
				array_push( $users[$cont]["sistemas"], array(
					"idsistema"   => $value->idsistema,
					"sistema"     => $value->sistema,
					"sistDesc"    => $value->sistDesc,
					"sistUrl"     => $value->sistUrl,
					"sistUrlSample" => $value->sistUrlSample,
					"sistActivo"  => $value->sistActivo,
					"sistPrefijo" => $value->sistPrefijo,
					"sistScreen"  => $value->sistScreenshot,
					"idrol"       => $value->idrol,
					"rol"         => $value->rol
				) );
			}
		} else
			$users = array();

		return $users;
	}

	/**
	 * [getIncidentManagers description]
	 * @return [type] [description]
	 */
	function getIncidentManagers() {
		return $this->db
					->select("user.idwom_usuario AS idipwUsuarios")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS ipwArea_idipwArea")
					->select("area.nombre AS area")
					->select("user.activo")
					->from("wom_usuario user")
					->join("wom_usuarioconfig cnfg", "cnfg.wom_usuario_idwomuser = user.idwom_usuario")
					->join("wom_configopciones optn", "optn.wom_configsistema_idwom_configsistema = 3 AND optn.idwom_configopciones = cnfg.wom_configopciones_idwom_configopciones")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area")
					->where("cnfg.wom_configsistema_idwom_configsistema", 3)
					->where("optn.valor", 1)
					->get()->result();
	}

	/**
	 * [getUsersByArea description]
	 * @param  [type] $idarea [description]
	 * @return [type]         [description]
	 */
	function getUsersByArea( $idarea, $all = false ) {
		if( !$all )
			$this->db->where("area.idwom_area", $idarea);
		return $this->db
					->select("user.idwom_usuario AS id")
					->select("user.idwom_usuario AS id_usuario")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS idarea")
					->select("area.nombre AS area")
					->select("user.activo")
					->from("wom_usuario user")
					->join("wom_area area", "area.idwom_area = user.wom_area_idwom_area")
					->where("user.activo !=", 0)
					->order_by("user.email ASC")
					->get()->result();
	}

	function updateUser( $iduser, $update ) {
		$this->db->where( $this->key, $iduser );
		if( $this->db->update( $this->table, $update ) )
			return true;
		return false;
	}

	function updatePass( $iduser, $pass ) {
		$this->db->where( $this->key, $iduser );
		$update = array( "password" => sha1( $pass ) );
		if( $this->db->update( $this->table, $update ) )
			return true;
		return false;
	}

	function updateToken( $token = null, $idUser = null ) {
		if (!is_null($token)){
			$this->db->where( "token", $token );
		}
		if (!is_null($idUser)){
			$this->db->where( "wom_usuario_idwom_usuario", $idUser );
		}

		$update = array( "activo" => 0 );
		if( $this->db->update( "wom_reset_tokens", $update ) )
			return true;
		return false;
	}
	/**
	 * [getConfigByID description]
	 * @param  [type] $idSystem [description]
	 * @return [type]           [description]
	 */
	function getConfigByID( $idSystem ) {
		return $this->db
					->from("wom_usuarioconfig")
					->where("idwom_usuarioconfig", $idSystem)
					->get()->result();
	}

	function getSessionApp( $iduser ) {
		$aux = $this->db
					->select("usu.*")
					->select("rol.idwom_rol")
					->from($this->table." usu")
					->join("wom_usuariorol urol", "usu.idwom_usuario = urol.wom_usuario_idwom_usuario")
					->join("wom_rol rol", "urol.wom_rol_idwom_rol = rol.idwom_rol")
					->where($this->key, $iduser)
					->where("rol.wom_sistema_idwom_sistema", 7)
					->get()->result();

		$permisosApp = $this->db
					->select("prms.idwom_permiso AS id", false)
					->from("wom_rol rlus")
					->join("wom_rolpermiso rlpm", "rlpm.wom_rol_idwom_rol = rlus.idwom_rol")
					->join("wom_permiso prms", "prms.idwom_permiso = rlpm.wom_permiso_idwom_permiso")
					->where("rlus.idwom_rol", $aux[0]->idwom_rol)
					->get()->result();

		$permisosApp = objectToArray($permisosApp);
		$permisosApp = array_map('array_pop', $permisosApp);
		asort($permisosApp);
		$permisosApp = implode(',', $permisosApp);


		$aux[0]->permisos = $permisosApp;

		return $aux;
	}


	/**
	 * [getUserByIDSpec description]
	 * @param  [type] $iduser [description]
	 * @return [type]         [description]
	 */
	function getUserByIDSpec( $iduser ) {
		$aux = $this->db
					->select("user.idwom_usuario AS id")
					->select("user.nombre")
					->select("user.email")
					->select("user.telefono")
					->select("user.wom_area_idwom_area AS idarea")
					->select("user.activo")
					->from("wom_usuario user")
					->where("user.idwom_usuario", $iduser)
					->get()->result();

		if( count( $aux ) > 0 ) {
			$user = array(
				"id"       => $aux[0]->id,
				"nombre"   => $aux[0]->nombre,
				"email"    => $aux[0]->email,
				"telefono" => $aux[0]->telefono,
				"idarea"   => $aux[0]->idarea,
				"activo"   => $aux[0]->activo
			);
		} else
			$user = array();

		return $user;
	}

	/**
	 * [doRandomPass description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	function doRandomPass( $id ) {
		$newpass = self::PASS_PREFIX.rand(1000, 9999);
		$aux = $this->db
					->select("user.idwom_usuario AS id")
					->select("user.nombre")
					->select("user.email")
					->from("wom_usuario user")
					->where("user.idwom_usuario", $id)
					->get()->result();

		$this->db->flush_cache();

		$this->db->set("password", sha1($newpass));
		$this->db->where("idwom_usuario", $id);
		$this->db->update("wom_usuario");

		$return = array(
			"nombre"  => $aux[0]->nombre,
			"email"   => $aux[0]->email,
			"newpass" => $newpass
		);

		return $return;
	}

	/**
	 * [newUser description]
	 * @author Oscar García Chávez <oscar.garcia@consultor.wom.cl>
	 * @version 1.0	Version Base
	 * @param   [type]      $infoUser [description]
	 * @return  [type]                [description]
	 */
	function newUser($infoUser){

		$usuarioFLM = false;
		$lastID = null;

		$newPass = self::PASS_PREFIX.rand(1000, 9999);
		$infoUser["password"] = sha1($newPass);

		$sistemas = $infoUser["sistemas"];
		$roles = $infoUser["roles"];
		$grupos = (isset($infoUser["grupos"]))?$infoUser["grupos"]:array();
		unset($infoUser["sistemas"]);
		unset($infoUser["roles"]);
		unset($infoUser["grupos"]);

		if (isset($infoUser["flmr_zonas_id_flmr_zonas"])) {
			$usuarioFLM = true;
			$zona = $infoUser["flmr_zonas_id_flmr_zonas"];
			$empresa = $infoUser["flmr_contratistas_idflmr_contratistas"];
			unset($infoUser["flmr_zonas_id_flmr_zonas"]);
			unset($infoUser["flmr_contratistas_idflmr_contratistas"]);
		}

		// Se inserta información básica del usuario
		if ($this->db->insert("wom_usuario", $infoUser)) {
			$lastID = $this->db->insert_id();
		}else{
			return false;
		}

		// Si el usuario fue insertado con éxito se procede a insertar la demás información
		if (!is_null($lastID)) {
			// Se insertan sistemas del usuario
			foreach ($sistemas as $sistema) {
				$this->db->insert("wom_usuariosistema", array("wom_usuario_idwomuser" => $lastID, "wom_sistema_idwom_system" => $sistema));
			}
			// Se insertan roles del usuario
			foreach ($roles as $rol) {
				$this->db->insert("wom_usuariorol", array("wom_usuario_idwom_usuario" => $lastID, "wom_rol_idwom_rol" => $rol));
			}
			// Se insertan grupos del usuario
			foreach ($grupos as $grupo) {
				$this->db->insert($this->schemaTickets.".tt_usuariogrupousuario", array("tt_usuario_idtt_usuario" => $lastID, "tt_grupousuario_idtt_grupoUsuario" => $grupo));
			}
			// Si es usuario FLM se inserta información específica para FLM
			if ($usuarioFLM) {
				$this->db->insert($this->schemaFLM.".flmr_usuarioszonas", array("usuario_id_usuario" => $lastID, "flmr_zonas_id_flmr_zonas" => $zona));
				$this->db->insert($this->schemaFLM.".flmr_usuarioscontratistas", array("usuario_id_usuario" => $lastID, "flmr_contratistas_idflmr_contratistas" => $empresa));
			}

			$infoUser["password"] = $newPass;
			$this->sendNewUserNotification($infoUser);

			return true;
		}
	}

	/**
	 * [modUserFLM description]
	 * @author Oscar García Chávez <oscar.garcia@consultor.wom.cl>
	 * @version 1.0	Version Base
	 * @param   [type]      $idUser   [description]
	 * @param   [type]      $infoUser [description]
	 * @return  [type]                [description]
	 */
	function modUserFLM($idUser, $infoUser){

		$newTicketFLMUser = (intval($infoUser["newTicketFLMUser"]));
		unset($infoUser["newTicketFLMUser"]);

		// Se extrae la data de tablas adicionales y se deja solo la data básica del usuario
		$sistemas = $infoUser["sistemas"];
		unset($infoUser["sistemas"]);
		$roles = $infoUser["roles"];
		unset($infoUser["roles"]);
		$grupos = (isset($infoUser["grupos"]))?$infoUser["grupos"]:array();
		unset($infoUser["grupos"]);
		$zona = $infoUser["flmr_zonas_id_flmr_zonas"];
		unset($infoUser["flmr_zonas_id_flmr_zonas"]);
		$empresa = $infoUser["flmr_contratistas_idflmr_contratistas"];
		unset($infoUser["flmr_contratistas_idflmr_contratistas"]);

		$gruposActuales = $this->db
							->select("ugru.tt_grupousuario_idtt_grupoUsuario")
							->from($this->schemaTickets.".tt_usuariogrupousuario ugru")
							->where("ugru.tt_usuario_idtt_usuario", $idUser)
							->get()->result();

		$gruposActuales = array_map(function($x){ return $x["tt_grupousuario_idtt_grupoUsuario"]; }, objectToArray($gruposActuales));

		$sistemasActuales = $this->db
							->select("rol.wom_sistema_idwom_sistema")
							->from("wom_usuariorol urol")
							->join("wom_rol rol", "urol.wom_rol_idwom_rol = rol.idwom_rol")
							->where("urol.wom_usuario_idwom_usuario", $idUser)
							->where_in("rol.wom_sistema_idwom_sistema", array(2,4,6,7))
							->get()->result();

		$sistemasActuales = array_map(function($x){ return $x["wom_sistema_idwom_sistema"]; }, objectToArray($sistemasActuales));

		$rolsToDelete = $this->db
								->select("idwom_rol")
								->from("wom_rol")
								->where("wom_sistema_idwom_sistema", 7)
								->get()->result();

		$rolsToDelete = array_map(function($x){ return $x["idwom_rol"]; }, objectToArray($rolsToDelete));

		// Se actualiza información básica del usuario
		$this->db->where("idwom_usuario", $idUser);
		if (!$this->db->update("wom_usuario", $infoUser)) {
			return false;
		}else{
			// Elimino rol actual de FLM previo a insert de nuevo rol
			$this->db->where("wom_usuario_idwom_usuario", $idUser);
			$this->db->where_in("wom_rol_idwom_rol", $rolsToDelete);
			$this->db->delete('wom_usuariorol');
			// newTicketFLMUser: Si es un usuario es nuevo en grupo FLM, si se mantiene o si es eliminado del grupo FLM:
			if ($newTicketFLMUser==0) {
				$roles = array(0 => $roles[0]);
			}else{
				if ($newTicketFLMUser==1) {
					// Se insertan sistemas del usuario
					foreach ($sistemas as $sistema) {
						if (!in_array($sistema, $sistemasActuales)) {
							$doInsertSistema = true;
						}else{
							$doInsertSistema = false;
						}

						if ($doInsertSistema) {
							$this->db->insert("wom_usuariosistema", array("wom_usuario_idwomuser" => $idUser, "wom_sistema_idwom_system" => $sistema));
						}
					}
					// Si el user tiene el grupo 100 (ELIMINADOS) entonces lo actualizo al grupo 1 (FLM)
					if(!empty($gruposActuales) && in_array(100, $gruposActuales)){
						$this->db->where("tt_usuario_idtt_usuario", $idUser);
						$this->db->where("tt_grupousuario_idtt_grupoUsuario", 100);
						$this->db->update($this->schemaTickets.".tt_usuariogrupousuario", array("tt_grupousuario_idtt_grupoUsuario" => 1));
					}else{
					// Se insertan grupos del usuario, en este caso solo el grupo 1 (FLM)
						foreach ($grupos as $grupo) {
							$this->db->insert($this->schemaTickets.".tt_usuariogrupousuario", array("tt_usuario_idtt_usuario" => $idUser, "tt_grupousuario_idtt_grupoUsuario" => $grupo));
						}
					}
				}else{
					// Si el user tiene mas de 2 o más grupos y uno de esos es el grupo 1 (FLM), entonces solo elimino el grupo 1 (FLM)
					if (count($gruposActuales)>1 && in_array(1, $gruposActuales)) {
						$this->db->where("tt_usuario_idtt_usuario", $idUser);
						$this->db->where("tt_grupousuario_idtt_grupoUsuario", 1);
						$this->db->delete($this->schemaTickets.".tt_usuariogrupousuario");
					}
					// Se actualiza el grupo FLM a grupo 100 (ELIMINADOS)
					$this->db->where("tt_usuario_idtt_usuario", $idUser);
					$this->db->where("tt_grupousuario_idtt_grupoUsuario", 1);
					$this->db->update($this->schemaTickets.".tt_usuariogrupousuario", array("tt_grupousuario_idtt_grupoUsuario" => 100));
				}
			}
			// Roles
			// Se comprueba que los roles a insertar no pertenezcan a un sistema del que el usuario ya tiene un rol anterior

			foreach ($roles as $rol) {
				$sisRol = $this->db
							->select("rol.wom_sistema_idwom_sistema")
							->from("wom_rol rol")
							->where("rol.idwom_rol", $rol)
							->get()->result();

				if (!in_array($sisRol[0]->wom_sistema_idwom_sistema, $sistemasActuales) || $sisRol[0]->wom_sistema_idwom_sistema==7) {
					$doInsertRol = true;
				}else{
					$doInsertRol = false;
				}

				if ($doInsertRol) {
					$this->db->insert("wom_usuariorol", array("wom_usuario_idwom_usuario" => $idUser, "wom_rol_idwom_rol" => $rol));
				}
			}

			// Zona
			$this->db->where("usuario_id_usuario", $idUser);
			$this->db->update($this->schemaFLM.".flmr_usuarioszonas", array("flmr_zonas_id_flmr_zonas" => $zona));
			// Empresa
			$this->db->where("usuario_id_usuario", $idUser);
			$this->db->update($this->schemaFLM.".flmr_usuarioscontratistas", array("flmr_contratistas_idflmr_contratistas" => $empresa));

			return true;
		}
	}

	/**
	 * [sendNewUserNotification description]
	 * @author Oscar García Chávez <oscar.garcia@consultor.wom.cl>
	 * @version 1.0	Version Base
	 * @param   [type]      $post_array [description]
	 * @return  [type]                  [description]
	 */
	function sendNewUserNotification($post_array){
		$this->load->library("emailwom");
		$data["account"] = $post_array;
		$msg = $this->load->view('email/newUser', $data, true);
		$titulo = "Credenciales WOM PORTAL";
		$mailAsignado = $post_array["email"];
		$this->emailwom->doEmail( $msg, $titulo, $mailAsignado );
		return true;
	}

	/**
	 * [updateRandomPassByUser description]
	 * @author Oscar García Chávez <oscar.garcia@consultor.wom.cl>
	 * @version 1.0	Version Base
	 * @param   [type]      $data [description]
	 * @return  [type]            [description]
	 */
	function updateRandomPassByUser($data) {
		$this->load->library("emailwom");
		// Actualiza la contraseña y retorna la informacion para el envio de correo
		$newPass = $this->doRandomPass( $data["id"] );
		// Se envia el correo de notificacion
		$data["newPass"] = $newPass;
		$msg = $this->load->view('email/randomUpdate', $data, true);
		$titulo = "Actualizacion de Credenciales";
		$mailAsignado = $newPass["email"];
		$this->emailwom->doEmail( $msg, $titulo, $mailAsignado );
		return true;
	}
}
