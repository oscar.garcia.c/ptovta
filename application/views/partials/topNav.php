<link href="<?=assets_url()?>css/topnav.css?ver=<?=rand()?>" rel="stylesheet" type="text/css">
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header linkLogo">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?=base_url()?>"><img src="<?=assets_url()?>images/ptoventa_logo02.png" class="logo_ptovta" /></a>
            <a class="navbar-brand" href="<?=base_url()?>">Administración</a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-cog"></i> <?=$userName?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="<?=base_url()?>login/doLogout"><i class="fas fa-sign-out-alt"></i> Salir</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>