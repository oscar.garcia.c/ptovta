<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<li>
				<a href="<?=base_url()?>admin/usuarios"><i class="fa fa-user fa-fw"></i> Usuarios</a>
				<a href="<?=base_url()?>admin/clientes"><i class="fa fa-users fa-fw"></i> Clientes</a>
				<a href="<?=base_url()?>admin/productos"><i class="fa fa-mobile fa-fw"></i> Productos</a>
				<a href="<?=base_url()?>admin/familias"><i class="fa fa-object-group fa-fw"></i> Familias</a>
				<a href="<?=base_url()?>admin/proveedores"><i class="fa fa-user-md fa-fw"></i> Proveedores</a>
				<a href="<?=base_url()?>admin/mediospago"><i class="fa fa-credit-card fa-fw"></i> Medios de Pago</a>
				<a href="<?=base_url()?>admin/bodegas"><i class="fa fa-building fa-fw"></i> Bodegas</a>
				<a href="<?=base_url()?>admin/combosofertones"><i class="fa fa-star fa-fw"></i> Combos Ofertones</a>
				<a href="<?=base_url()?>admin/estadistica"><i class="fa fa-chart-bar fa-fw"></i> Estadísticas</a>
			</li>
		</ul>
	</div>
</div>