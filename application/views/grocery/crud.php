<?php foreach( $output["css_files"] as $file ): ?>
	<link type="text/css" rel="stylesheet" href="<?=$file?>" />
<?php endforeach; ?>
<?php foreach( $output["js_files"] as $file ): ?>
	<script src="<?=$file?>"></script>
<?php endforeach; ?>

<style type='text/css'>
body { font-family: Arial; font-size: 14px; }
a { /*color: blue;*/ text-decoration: none; font-size: 14px; }
a:hover { text-decoration: underline; }

h1 { margin-top: 20px !important; }

.form-control { height: 34px !important; padding: 6px 12px !important; }
div.chosen-search > input { width: 100% !important; }
a.chosen-single { height: 34px !important; padding: 4px 12px !important; }
abbr.search-choice-close { top: 10px !important; }
div > b { margin-top: 5px !important; }

.dataTables_wrapper,
div.form-content { font-family: monospace !important; font-size: 90% !important; }
a > span.ui-button-text { font-family: monospace !important; font-size: 100% !important; }

.report-div { overflow: hidden; }
</style>

<div class="row">
	<div class="col-md-12" style="padding-top: 20px;">
		<div class="col-lg-12">
			<h1 class="page-header"><?=$nombreTabla?></h1>
		</div>
		<?=$output["output"]?>
	</div>
</div>