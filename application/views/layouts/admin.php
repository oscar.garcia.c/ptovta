<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?=base_url()?>favicon.ico">
    <title>Punto Ventas</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=bower_url()?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- MetisMenu CSS -->
    <link href="<?=bower_url()?>metisMenu/dist/metisMenu.min.css" rel="stylesheet" type="text/css">
    <!-- Timeline CSS -->
    <link href="<?=bower_url()?>startbootstrap-sb-admin-2/dist/css/timeline.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?=bower_url()?>startbootstrap-sb-admin-2/dist/css/sb-admin-2.css" rel="stylesheet" type="text/css">
    <!-- Custom Fonts -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <!-- Fix Dashboard -->
    <link href="<?=assets_url()?>css/admin.css?ver=<?=rand()?>" rel="stylesheet" type="text/css">
    <!-- jQuery -->
    <script src="<?=bower_url()?>jquery/dist/jquery.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script>
    var PORTAL = {};
    PORTAL.base_url = "<?=base_url()?>";
    </script>
</head>
<body>
    <div id="wrapper">
        <?=$template['partials']['topNav']?>
        <?=$template['partials']['leftMenuAdmin']?>
        <div id="page-wrapper">
            <?=$template['body']?>
        </div>
    </div>
    <!-- Footer -->
    <?=$template['partials']['footer']?>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=bower_url()?>bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=bower_url()?>metisMenu/dist/metisMenu.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?=bower_url()?>startbootstrap-sb-admin-2/dist/js/sb-admin-2.js"></script>
    <!-- Custom Logo Spin -->
    <script src="<?=assets_url()?>js/logoSpin.js?ver=<?=rand()?>"></script>
    <script type="text/javascript" src="<?=assets_url()?>js/alone.alertyfy.js"></script>
</body>
</html>
