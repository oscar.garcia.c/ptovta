<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct() {
		parent::__construct();

		// Se redirecciona al dashboard por defecto en caso de querer entrar a cualquier metodo (excepto para salir)
		//  para evitar problemas de ejecucion
		if( $this->session->admLogged )
			if( $this->router->fetch_method() != "doLogout" )
				redirect('/admin', 'refresh');
	}

	/**
	 * [index description]
	 * @author Oscar García Chávez <oscar.garcia@wom.cl>
	 * @version 1.0	Version Base
	 * @date    2018-11-16
	 * @return  [type]      [description]
	 */
	public function index() {
		$this->template->set_layout("login");
		$this->template->build("login/index", $this->data);
	}

	/**
	 * [doLogin description]
	 * @author Oscar García Chávez <oscar.garcia@wom.cl>
	 * @version 1.0	Version Base
	 * @date    2018-11-16
	 * @return  [type]      [description]
	 */
	public function doLogin() {
		if( $this->input->post() ) {
			$this->load->model("usuarios_model", "usuarios");
			$validate = $this->usuarios->checkLogin( $this->input->post("email"), $this->input->post("password") );

			if( $validate["ok"] ) {
				$sess = array(
					"userLogged"	=> true,
					"userName"		=> $validate["data"]->nombre,
					"userTipo"		=> $validate["data"]->tipoUsuarioNombre,
					"userTipoId"		=> $validate["data"]->tipoUsuarioID
				);

				$this->session->set_userdata( $sess );

				switch ($this->session->userTipoId) {
					case 1:
						redirect('/Admin', 'refresh');
						break;
					case 2:
						# code...
						break;
					case 3:
						# code...
						break;
				}
			} else{
				redirect('/login', 'refresh');
			}
		} else{
			redirect('/login', 'refresh');
		}
	}

	/**
	 * Metodo para salir del portal una vez logeado
	 * @author Jose Vera D. <jose.vera@synergistics.cl>
	 * @version 1.0 	Version inicial del termino de sesion
	 * @return void 	Redirecciona a al metodo de login
	 */
	public function doLogout() {
		$killThemAll = array( "userLogged", "userName", "userTipo" );
		$this->session->unset_userdata( $killThemAll );

		redirect('/login', 'refresh');
	}
}