<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	function __construct() {
		parent::__construct();

		// Valido que el usuario tiene permiso para hacer cosas de admin
		// if( !searchPermiso( 1, $this->session->admPermisos ) )
			// show_error('No tienes permiso para entrar en esta pagina.', 500, "Permisos Necesarios" );

		$this->template->set_layout("admin");
		$this->load->library('grocery_CRUD');

		// Se cera el objeto que se utiliza para crear el CRUD
		$this->crud = new grocery_CRUD();

		// Seteo el lenguaje de la tabla a español
		$this->crud->set_language("spanish");
		// Seteo el tema que se usara en las tablas
		$this->crud->set_theme('datatables');

		// Borro de la tabla el boton para exportar, para imprimir, para borrar registros y la carga de jquery (para que no se repita)
		$this->crud->unset_export()->unset_print()->unset_delete()->unset_jquery();
	}

	public function index() {
		$this->template->build('admin/index', $this->data);
	}

	function _buildTemplate( $output, $nombreTabla  ) {
		$this->data["output"] = (array)$output;
		$this->data["nombreTabla"] = $nombreTabla;
		$this->template->build('grocery/crud', $this->data);
	}

	/**
	 * [_doSha description]
	 * @param  [type] $password [description]
	 * @return [type]           [description]
	 */
	function _doSha( $password ) {
		return sha1( $password );
	}

	/**
	 * [usuarios description]
	 * @return [type] [description]
	 */
	public function usuarios() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('usuarios');
			// Seteo las relaciones que tiene la tabla
			// $this->crud->set_relation('wom_area_idwom_area', 'wom_area', 'nombre');

			// $this->crud->set_relation_n_n('roles', 'wom_usuariorol', 'wom_rol', 'wom_usuario_idwom_usuario', 'wom_rol_idwom_rol', 'nombre');
			// $this->crud->set_relation_n_n('sistemas', 'wom_usuariosistema', 'wom_sistema', 'wom_usuario_idwomuser', 'wom_sistema_idwom_system', 'nombre');
			// $this->crud->set_relation_n_n('gruposticket', 'coreticket.tt_usuariogrupousuario', 'coreticket.tt_grupousuario', 'tt_usuario_idtt_usuario', 'tt_grupousuario_idtt_grupoUsuario', 'nombre');
			// $this->crud->set_relation_n_n('zonaflm', 'flmreport.flmr_usuarioszonas', 'flmreport.flmr_zonas', 'usuario_id_usuario', 'flmr_zonas_id_flmr_zonas', 'nombre');

			// $this->crud->set_primary_key('idwom_empresa','womempresas.wom_view_empresasflm'); //Para poder usar una vista en GROCERY CRUD se designa la primary key
			// $this->crud->set_relation_n_n('contratistaflm', 'flmreport.flmr_usuarioscontratistas', 'womempresas.wom_view_empresasflm', 'usuario_id_usuario', 'flmr_contratistas_idflmr_contratistas', 'empresa');
			// $this->crud->set_relation_n_n('contratistaflm', 'flmreport.flmr_usuarioscontratistas', 'flmreport.flmr_contratistas', 'usuario_id_usuario', 'flmr_contratistas_idflmr_contratistas', 'empresa');

			// Seteo un titulo para la tabla
			$this->crud->set_subject('Usuario');
			// Seteo las columnas que se mostraran en la tabla
			// $this->crud->columns('nombre', 'email', 'telefono', 'wom_area_idwom_area', 'receiveMail', 'activo');
			// Seteo los campos que se utilizara para editar/agregar un registro
			// $this->crud->fields('nombre', 'email', 'telefono', 'wom_area_idwom_area', 'password', 'receiveMail', 'activo', 'roles', 'sistemas', 'gruposticket', 'zonaflm', 'contratistaflm');
			// Se setean validaciones para los campos
			// $this->crud->set_rules("nombre", "Nombre", "required")
			// 			->set_rules("email", "Email", "required")
			// 			->set_rules("receiveMail", "¿Recibe Mails?", "required")
			// 			->set_rules("wom_area_idwom_area", "Area", "required")
			// 			->set_rules("activo", "Activo", "required");
			// Se setea el campo de password como password
			// $this->crud->change_field_type('password', 'password');
			// Seteo los nombres que quiero que muestren los campos
			// $this->crud->display_as('wom_area_idwom_area', 'Área');
			// $this->crud->display_as('gruposticket', 'Grupos Ticket');
			// $this->crud->display_as('zonaflm', 'Zona FLM');
			// $this->crud->display_as('contratistaflm', 'Contratista FLM');
			// $this->crud->display_as('receiveMail', '¿Recibe Mails?');
			// Campos especiales con "0" o "1"
			// $this->crud->field_type('activo', 'true_false', array( 'No', 'Si' ));
			// $this->crud->field_type('receiveMail','dropdown',
			// 	array(
			// 		'1' => 'Sí',
			// 		'-1' => 'Vacaciones',
			// 		'0' => 'No'
			// 	)
			// );
			// Arreglos para la funcionalidad de la contraseña
			// $this->crud->callback_edit_field('password', array($this,'setPasswordInputToEmpty') );
			// $this->crud->callback_add_field('password',array($this,'setPasswordInputToEmpty'));

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			// $this->session->set_userdata("updateInsertTable", "wom_usuario");
			// Metodo que se ejecutara siempre antes del update para dejar log
			// $this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();
			$nombreTabla = "Administrar Usuarios";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * Metodo para dejar registro de las actualizaciones/inserciones de informacion en el sistema
	 * @param  [type] $post_array  [description]
	 * @param  [type] $primary_key [description]
	 * @return [type]              [description]
	 */
	function logAfterUpdate( $post_array, $primary_key ) {
		// Encrypt password only if is not empty. Else don't change the password to an empty field
		if( !empty( $post_array['password'] ) ) {
			$post_array['password'] = $this->_doSha( $post_array['password'] );
		} else {
			unset( $post_array['password'] );
		}

		$aux = array();
		switch( $this->session->updateInsertTable ) {
			case "wom_area":
				$this->load->model("admareas_model", "area");
				$aux = $this->area->getAreaByID( $primary_key );
				break;
			case "wom_sistema":
				$this->load->model("admsistemas_model", "sistema");
				$aux = $this->sistema->getSistemaByID( $primary_key );
				$aux = objectToArray( $aux[0] );
				break;
			case "wom_permiso":
				$this->load->model("admpermisos_model", "permisos");
				$aux = $this->permisos->getPermisoByID( $primary_key );
				break;
			case "wom_usuario":
				$this->load->model("admusuarios_model", "usuarios");
				$aux = $this->usuarios->getUserByID( $primary_key );
				break;
			case "wom_configsistema":
				$this->load->model("admconfigsistema_model", "configsys");
				$aux = $this->configsys->getConfigByID( $primary_key );
				break;
			case "wom_configopciones":
				$this->load->model("admconfigopcion_model", "configsys");
				$aux = $this->configsys->getConfigByID( $primary_key );
				break;
			case "wom_usuarioconfig":
				$this->load->model("admusuarios_model", "usuarios");
				$aux = $this->usuarios->getConfigByID( $primary_key );
				break;
		}

		if( !empty( $aux ) )
			foreach( $post_array as $key => $value ) {
				switch( $key ) {
					case "wom_area_idwom_area":
						$auxKey = "area"; break;
					default:
						$auxKey = $key;
				}
				if( isset($aux[$auxKey]) && !is_array($value) && $value != $aux[$auxKey] ) {
					$insert = array(
						"wom_controltipo_idwom_controltipo" => 2,
						"tabla"                             => $this->session->updateInsertTable,
						"campo"                             => $key,
						"idregistro"                        => $primary_key,
						"valor"                             => $value
					);
					$this->controlc->insertCC( $insert );
				}
			}

		$this->session->unset_userdata("updateInsertTable");
		return $post_array;
	}

	/**
	 * [logAfterInsert description]
	 * @param  [type] $post_array  [description]
	 * @param  [type] $primary_key [description]
	 * @return [type]              [description]
	 */
	function logAfterInsert( $post_array, $primary_key ) {

		/*Send notification*/
		if( empty( $post_array['password'] ) ) {
			$post_array['password'] = $this->randomKey(6);		
		}

		if ($post_array['receiveMail'] == 1){
			$this->load->model("admusuarios_model", "users");
			$this->users->sendNewUserNotification($post_array);
		}
        /*FIN Send notification*/

		// Encrypt password only if is not empty. Else don't change the password to an empty field
		if( !empty( $post_array['password'] ) ) {
			$post_array['password'] = $this->_doSha( $post_array['password'] );
		} else {
			unset( $post_array['password'] );
		}

		$insert = array(
			"wom_controltipo_idwom_controltipo" => 1,
			"tabla" => $this->session->updateInsertTable
		);
		$aux = $this->controlc->insertCC( $insert );

		$this->session->unset_userdata("updateInsertTable");
		return $post_array;
	}
	function randomKey($length) {
	    $pool = array_merge(range(0,9), range('a', 'z'),range('A', 'Z'));
	    $key = "";
	    for($i=0; $i < $length; $i++) {
	        $key .= $pool[mt_rand(0, count($pool) - 1)];
	    }
	    return $key;
	}
	/**
	 * [areas description]
	 * @return [type] [description]
	 */
	public function areas() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('wom_area');
			// Seteo un titulo para la tabla
			$this->crud->set_subject('Áreas');
			// Seteo las columnas que se mostraran en la tabla
			$this->crud->columns('nombre', 'activo');
			// Seteo los campos que se utilizara para editar/agregar un registro
			$this->crud->fields('nombre', 'activo');
			// Campos especiales con "0" o "1"
			$this->crud->field_type('activo', 'true_false', array( 'Desactivo', 'Activo' ));

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			$this->session->set_userdata("updateInsertTable", "wom_area");
			// Metodo que se ejecutara siempre antes del update para dejar log
			$this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();

			$nombreTabla = "Administrar &Aacute;reas";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * [sistemas description]
	 * @return [type] [description]
	 */
	public function sistemas() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('wom_sistema');
			// Seteo un titulo para la tabla
			$this->crud->set_subject('Sistemas');
			// Seteo las columnas que se mostraran en la tabla
			$this->crud->columns('nombre', 'descripcion', 'url', 'activo');
			// Seteo los campos que se utilizara para editar/agregar un registro
			$this->crud->fields('nombre', 'descripcion', 'url', 'activo', 'prefijo', 'privado', 'icon');
			// Campos especiales con "0" o "1"
			$this->crud
					->field_type('activo', 'true_false', array( 'Desactivo', 'Activo' ))
					->field_type('privado', 'true_false', array( 'No', 'Si' ));

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			$this->session->set_userdata("updateInsertTable", "wom_sistema");
			// Metodo que se ejecutara siempre antes del update para dejar log
			$this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();

			$nombreTabla = "Administrar Sistemas";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * [permisos description]
	 * @return [type] [description]
	 */
	public function permisos() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('wom_permiso');
			// Seteo las relaciones que tiene la tabla
			$this->crud
				->set_relation('wom_tipopermiso_idwom_tipopermiso', 'wom_tipopermiso', 'nombre');
			// Seteo un titulo para la tabla
			$this->crud->set_subject('Permisos');
			// Seteo las columnas que se mostraran en la tabla
			$this->crud->columns('nombre', 'descripcion', 'wom_tipopermiso_idwom_tipopermiso');
			// Seteo los campos que se utilizara para editar/agregar un registro
			$this->crud->fields('nombre', 'descripcion', 'wom_tipopermiso_idwom_tipopermiso');
			// Seteo los nombres que quiero que muestren los campos
			$this->crud
				->display_as('wom_tipopermiso_idwom_tipopermiso', 'Tipo Permiso');

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			$this->session->set_userdata("updateInsertTable", "wom_permiso");
			// Metodo que se ejecutara siempre antes del update para dejar log
			$this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();

			$nombreTabla = "Administrar Permisos";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * [configsistemas description]
	 * @return [type] [description]
	 */
	public function configsistemas() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('wom_configsistema');
			// Seteo un titulo para la tabla
			$this->crud->set_subject('Configuracion Sistema');
			// Seteo las relaciones que tiene la tabla
			$this->crud
				->set_relation('wom_sistema_idwom_system', 'wom_sistema', 'nombre')
				->set_relation('wom_configtipo_idwom_configtipo', 'wom_configtipo', 'nombre');
			// Seteo las columnas que se mostraran en la tabla
			$this->crud->columns('nombre', 'wom_sistema_idwom_system', 'wom_configtipo_idwom_configtipo');
			// Seteo los campos que se utilizara para editar/agregar un registro
			$this->crud->fields('nombre', 'wom_sistema_idwom_system', 'wom_configtipo_idwom_configtipo');
			// Seteo los nombres que quiero que muestren los campos
			$this->crud
				->display_as('wom_sistema_idwom_system', 'Sistema')
				->display_as('wom_configtipo_idwom_configtipo', 'Tipo Campo');

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			$this->session->set_userdata("updateInsertTable", "wom_configsistema");
			// Metodo que se ejecutara siempre antes del update para dejar log
			$this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();

			$nombreTabla = "Administrar Atributos Extras para Usuarios";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * [configopciones description]
	 * @return [type] [description]
	 */
	public function configopciones() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('wom_configopciones');
			// Seteo un titulo para la tabla
			$this->crud->set_subject('Configuracion Opciones');
			// Seteo las relaciones que tiene la tabla
			$this->crud
				->set_relation('wom_configsistema_idwom_configsistema', 'wom_configsistema', 'nombre');
			// Seteo las columnas que se mostraran en la tabla
			$this->crud->columns('wom_configsistema_idwom_configsistema', 'opcion', 'valor');
			// Seteo los campos que se utilizara para editar/agregar un registro
			$this->crud->fields('wom_configsistema_idwom_configsistema', 'opcion', 'valor');
			// Seteo los nombres que quiero que muestren los campos
			$this->crud
				->display_as('wom_configsistema_idwom_configsistema', 'Atributo');

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			$this->session->set_userdata("updateInsertTable", "wom_configopciones");
			// Metodo que se ejecutara siempre antes del update para dejar log
			$this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();

			$nombreTabla = "Configurar Atributos Extras para Usuarios";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * [usuarioconfig description]
	 * @return [type] [description]
	 */
	public function usuarioconfig() {
		try {
			// Seteo la tabla principal que se esta editando
			$this->crud->set_table('wom_usuarioconfig');
			// Seteo un titulo para la tabla
			$this->crud->set_subject('Opciones Extras');
			// Seteo las relaciones que tiene la tabla
			$this->crud
				->set_relation('wom_configsistema_idwom_configsistema', 'wom_configsistema', 'nombre')
				->set_relation('wom_usuario_idwomuser', 'wom_usuario', 'nombre')
				->set_relation('wom_configopciones_idwom_configopciones', 'wom_configopciones', 'opcion');
			// Seteo las columnas que se mostraran en la tabla
			$this->crud->columns('wom_configsistema_idwom_configsistema', 'wom_usuario_idwomuser', 'wom_configopciones_idwom_configopciones');
			// Seteo los campos que se utilizara para editar/agregar un registro
			$this->crud->fields('wom_configsistema_idwom_configsistema', 'wom_usuario_idwomuser', 'wom_configopciones_idwom_configopciones');
			// Seteo los nombres que quiero que muestren los campos
			$this->crud
				->display_as('wom_configsistema_idwom_configsistema', 'Atributo Extra')
				->display_as('wom_usuario_idwomuser', 'Usuario')
				->display_as('wom_configopciones_idwom_configopciones', 'Valor Atributo');

			// Session para entregar como parametro a metodo que crea log de actualizacion/insercion de data
			$this->session->set_userdata("updateInsertTable", "wom_usuarioconfig");
			// Metodo que se ejecutara siempre antes del update para dejar log
			$this->crud->callback_before_update( array( $this, "logAfterUpdate" ) );
			// $this->crud->callback_before_insert( array( $this, "logAfterInsert" ) );

			// Se crea el render de la tabla
			$output = $this->crud->render();

			$nombreTabla = "Configurar Atributos Extras en Usuarios";
			$this->_buildTemplate( $output, $nombreTabla );
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	/**
	 * [grupos description]
	 * @return [type] [description]
	 */
	public function grupos() {
		$this->load->model("Admgrupos_model", "grupos");

		$listado = $this->grupos->getGrupos();

		$this->data['listado'] = $listado;
		$this->template->build('admin/grupos', $this->data);
	}

	public function newgrupo(){
		//var_dump($this->data);
		$this->load->model("Admgrupos_model", "grupos");

		$listado		= $this->grupos->getGrupos();

		//var_dump($listado);
		$this->data['listado'] = $listado;

		$this->template->build('admin/newgrupo', $this->data);

	}

	public function infogrupousuario(){

		$this->load->model("Admgrupos_model", "grupos");

		$id = $this->input->post('id');

		$listausuario		= $this->grupos->getUsuariosGrupos($id);
		$usunoesta			= $this->grupos->getUsuariosGrupos($id,'!=');

		echo json_encode($listausuario);
	}

	/**
	 * [infousuariosnoestan description]
	 * @return [type] [description]
	 */
	public function infousuariosnoestan() {
		if( $this->input->post() ) {
			$this->load->model("Admgrupos_model", "grupos");
			$id = $this->input->post("id");
			$listano = $this->grupos->getUsuariosGrupos( $id, '!=' );
			die( json_encode( $listano ) );
		} else
			show_error( "No tienes permiso para entrar en esta pagina.", 500, "Permisos Necesarios" );
	}

	public function addusuariogrupo(){

		$this->load->model("Admgrupos_model", "grupos");

		$idusu	 = $this->input->post('idusu');
		$idgrupo = $this->input->post('idgrupo');
		$idc  	 = $this->input->post('idencargado');

		$resp = $this->grupos->insertusergrupo($idusu, $idgrupo, $idc);

		if($resp == 'ok'){
			$listausuario		= $this->grupos->getUsuariosGrupos($idgrupo);

			echo json_encode($listausuario);
		}else{
			echo $resp;
		}


	}

	public function updateencargado(){

		$this->load->model("Admgrupos_model", "grupos");


		$idusu	 = $this->input->post('idusu');
		$idgrupo = $this->input->post('idgrupo');
		$idc  	 = $this->input->post('idencargado');

		$resp = $this->grupos->updateencargado($idusu, $idgrupo, $idc);

		if($resp == 'ok'){
			$listausuario		= $this->grupos->getUsuariosGrupos($idgrupo);

			echo json_encode($listausuario);
		}else{
			echo $resp;
		}

	}

	public function quitarusuariogrupo(){

		$this->load->model("Admgrupos_model", "grupos");


		$idusu	 = $this->input->post('idusu');
		$idgrupo = $this->input->post('idgrupo');


		$resp = $this->grupos->quitarusuariogrupomodelo($idusu, $idgrupo);

		if($resp == 'ok'){
			$listausuario		= $this->grupos->getUsuariosGrupos($idgrupo);

			echo json_encode($listausuario);
		}else{
			echo $resp;
		}

	}

	public function agreganuevogrupo(){

		$this->load->model("Admgrupos_model", "grupos");

		$texto	 = $this->input->post('texto');

		$resp = $this->grupos->agreganuevogrupomodelo($texto);

		if($resp == 'ok'){
			$listagrupo		= $this->grupos->getGrupos();

			echo json_encode($listagrupo);
		}else{
			echo $resp;
		}
	}

	public function roles(){
		//var_dump($this->data);
		$this->load->model("Admroles_model", "sistema");

		$listasistema		= $this->sistema->getSistemas();
		$listausuario		= $this->sistema->getUsuarios();

		//var_dump($listausuario);
		$this->data['listasistema'] = $listasistema;
		//$this->data['listausuario'] = $listausuario;

		$this->template->build('admin/roles', $this->data);
	}

	public function getRolesXSistema(){

		$this->load->model("Admroles_model", "sistema");

		$ids	 	= $this->input->post('ids');
		$listaroles = $this->sistema->getRolesXSistemaModel($ids);

		echo json_encode($listaroles);

	}

	public function getPermisosXRol(){

		$this->load->model("Admroles_model", "sistema");

		$idr	 		= $this->input->post('idr');
		$listapermisos  = $this->sistema->getPermisosXRolModel($idr);

		echo json_encode($listapermisos);
	}

	public function getUsuariosXRol(){

		$this->load->model("Admroles_model", "sistema");

		$idr	 		= $this->input->post('idr');
		$listausuarios	= $this->sistema->getUsuariosXRolModel($idr);

		echo json_encode($listausuarios);
	}

	public function delRolesXSistema(){

		$this->load->model("Admroles_model", "sistema");

		$idrol	 		= $this->input->post('idrol');
		$idsistema	 	= $this->input->post('idsistema');

		$resp = $this->sistema->delRolesXSistemaModel($idrol,$idsistema);
		echo $resp;
		// if($resp == 'ok'){
		// 	//$listaroles = $this->sistema->getRolesXSistemaModel($idsistema);
		// 	//echo json_encode($listaroles);
		// }else{
		// 	//echo 'nok';
		// }
	}

	public function addRolesXSistema(){

		$this->load->model("Admroles_model", "sistema");

		$nombre    = $this->input->post('nombre');
		$idsistema = $this->input->post('idsis');

		$resp = $this->sistema->addRolesXSistemaModel($nombre, $idsistema);

		echo json_encode($resp);
	}

	public function addPermisosXRol(){

		$this->load->model("Admroles_model", "sistema");

		$nombre    	 = $this->input->post('nombre');
		$descripcion = $this->input->post('desc');
		$idtipoper 	 = $this->input->post('idtp');
		$idrol	 	 = $this->input->post('idrol');

		$resp = $this->sistema->addPermisosXRolModel($nombre, $descripcion, $idtipoper, $idrol);

		echo json_encode($resp);
	}

	public function newlistausuarios() {
		$this->load->model("Admroles_model", "sistema");

		$idr	 		= $this->input->post('idr');
		$listausuarios	= $this->sistema->listausuariosnoestanXRol( $idr );

		echo json_encode( $listausuarios );
	}

	public function addusurolususistema(){

		$this->load->model("Admroles_model", "sistema");

		$idusu	 		= $this->input->post('idusu');
		$idrol	 		= $this->input->post('idrol');
		$idsis	 		= $this->input->post('idsis');

		$listausuarios	= $this->sistema->addusurolususistemaModel($idusu,$idrol,$idsis);

		echo json_encode($listausuarios);

	}

	/**
	 * [setPasswordInputToEmpty description]
	 */
	function setPasswordInputToEmpty() {
		return "<input id='field-password' class='form-control' name='password' type='text' value='' placeholder='Dejar vacio para no editar'>";
	}

	/**
	 * [randpass description]
	 * @return [type] [description]
	 */
	function randpass() {
		$this->load->model("Admareas_model", "areas");

		$areas = $this->areas->getAreas();

		$this->data["areas"] = $areas;
		$this->template->build("admin/randompass", $this->data);
	}

	/**
	 * [getUserToRandPass description]
	 * @param  [type] $area [description]
	 * @return [type]       [description]
	 */
	function getUserToRandPass( $area = null ) {
		if( !is_null( $area ) ) {
			$this->load->model("Admusuarios_model", "usuario");
			if( $area == "all" )
				$users = $this->usuario->getUsersByArea( 1, true );
			else
				$users = $this->usuario->getUsersByArea( $area );
			die( json_encode( array( "valid" => true, "data" => $users ) ) );
		} else
			show_error('No tienes permiso para entrar en esta pagina.', 500, "Permisos Necesarios" );
	}

	/**
	 * [doUpdateRandPass description]
	 * @return [type] [description]
	 */
	function doUpdateRandPass() {
		if( $this->input->post() ) {
			$this->load->model("Admusuarios_model", "usuario");
			$this->load->library("emailwom");
			$toChange = $this->input->post("arr");
			foreach( $toChange as $value ) {
				// Actualiza la contraseña y retorna la informacion para el envio de correo
				$newPass = $this->usuario->doRandomPass( $value );
				// Se envia el correo de notificacion
				$this->data["newPass"] = $newPass;
				$msg = $this->load->view('email/randomUpdate', $this->data, true);
				$titulo = "Actualizacion de Credenciales";
				$mailAsignado = $newPass["email"];
				$this->emailwom->doEmail( $msg, $titulo, $mailAsignado );
			}
			die( json_encode( true ) );
		} else
			show_error('No tienes permiso para entrar en esta pagina.', 500, "Permisos Necesarios" );
	}

	function userpermisos() {

		$this->load->model("admpermisos_model", "permisos");
		$auxOne = $this->permisos->getPermisosByRol( $this->session->userdata("admRol") );
		$auxTwo = $this->permisos->getPermisosByRol( $this->session->userdata("ttwRol") );

		$this->template->build('admin/userpermiso', $this->data);
	}
}