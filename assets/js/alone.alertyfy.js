/**
 *	// Enviar un mensaje en pantalla (Tipo alert)
 *	$("body").messagefy({
 *		color: "btn-danger",
 *		title: "Error",
 *		titleIcon: "ok",
 *		msg: "<p>No se pudo borrar el registro.</p>"
 *	});
 *
 *	// Mostrar ventana de pregunta (tipo alert)
 *	$("body").questionfy({
 *		color: "btn-info",
 *		title: "Confirmaci&oacute;n de Borrado",
 *		titleIcon: "remove",
 *		msg: html,
 *		acceptText: "Borrar Borrador",
 *		acceptIcon: "floppy-remove",
 *		acceptID: "doDelete"
 *	});
 *
 *	// Mostrar ventana de "Cargando" con mensaje seleccionado
 *	loadingModal.showPleaseWait({ txt: "<p>Borrando borrador, por favor espere un momento.</p>" });
 *
 *	// Esconder ventana de "Cargando"
 *	loadingModal.hidePleaseWait();
 */

(function ( $ ) {
	/**
	 * [alertyfy description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	$.fn.alertyfy = function( options ) {
		var settings = $.extend({
			type: "warning",
			msg: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
		}, options );

		var alert = "<div class='alert alert-"+settings.type+" alert-dismissible fade in ninja' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+settings.msg+"</div>"
		this.append( alert ).find(".ninja").slideDown("fast").removeClass("ninja");
		return this;
	};

	/**
	 * [messagefy description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	$.fn.messagefy = function( options ) {
		var settings = $.extend({
			id: false,
			color: false,		// Options: btn-default, btn-primary, btn-success, btn-info, btn-warning, btn-danger
			size: false,		// Options: lg, sm
			title: "Titulo del mensaje",
			titleIcon: false,		// Options: false, Bootstrap 3 glyphs
			customFooterBtns : false, // Falso o html con botones
			msg: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
		}, options );

		var modal = "<div "+((settings.id)?"id='"+settings.id+"' ":"")+"class='modal fade tempModal'><div class='modal-dialog "+((settings.size)?"modal-"+settings.size:"")+"'><div class='modal-content'>";
		modal += "<div class='modal-header "+((settings.color)?settings.color:"")+"' style='border-top-left-radius: 5px; border-top-right-radius: 5px;'><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
		modal += "<h4 class='modal-title'>"+((settings.titleIcon)?"<span class='glyphicon glyphicon-"+settings.titleIcon+"' aria-hidden='true'></span> ":"")+settings.title+"</h4></div>";
		modal += "<div class='modal-body'><p>"+settings.msg+"</p></div>";
		if(!settings.customFooterBtns)
			modal += "<div class='modal-footer'><button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button></div>";
		else
			modal += "<div class='modal-footer'>"+(settings.customFooterBtns)+"</div>"
		modal += "</div></div></div>";

		this.append( modal );
		$('.tempModal').on('hidden.bs.modal', function (e) {
			$(this).remove();
		}).modal();

		return this;
	};

	/**
	 * [questionfy description]
	 * @param  {[type]} options [description]
	 * @return {[type]}         [description]
	 */
	$.fn.questionfy = function( options ) {
		var settings = $.extend({
			color: false,		// Options: btn-default, btn-primary, btn-success, btn-info, btn-warning, btn-danger
			size: false,		// Options: lg, sm
			title: "Titulo del mensaje",
			titleIcon: "question-sign",		// Options: false, Bootstrap 3 glyphs
			msg: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
			acceptID: false,
			acceptIcon: "ok",		// Options: false, Bootstrap 3 glyphs
			acceptText: "Continuar",
			acceptColor: "success",		// Options: default, primary, success, info, warning, danger, link
			cancelID: false,
			cancelIcon: "remove",		// Options: false, Bootstrap 3 glyphs
			cancelText: "Cancelar",
			cancelColor: "danger",		// Options: default, primary, success, info, warning, danger, link
			cancelShow: true,
			cruxShow: true
		}, options );

		var modal = "";
		modal += "<div class='modal fade tempQModal' tabindex='-1' role='dialog'><div class='modal-dialog "+((settings.size)?"modal-"+settings.size:"")+"'><div class='modal-content'>";
		modal += "<div class='modal-header "+((settings.color)?settings.color:"")+"' style='border-top-left-radius: 5px; border-top-right-radius: 5px;'>";
		if( settings.cruxShow )
			modal += "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
		modal += "<h4 class='modal-title'>"+((settings.titleIcon)?"<span class='glyphicon glyphicon-"+settings.titleIcon+"' aria-hidden='true'></span> ":"")+settings.title+"</h4></div>";
		modal += "<div class='modal-body'><p>"+settings.msg+"</p></div>";
		modal += "<div class='modal-footer'>";
		if( settings.cancelShow )
			modal += "<button type='button' class='btn btn-danger' data-dismiss='modal' "+((settings.cancelID)?"id='"+settings.cancelID+"'":"")+">"+((settings.cancelIcon)?"<span class='glyphicon glyphicon-"+settings.cancelIcon+"' aria-hidden='true'></span> ":"")+settings.cancelText+"</button>";
		modal += "<button type='button' class='btn btn-success' "+((settings.acceptID)?"id='"+settings.acceptID+"'":"")+">"+((settings.acceptIcon)?"<span class='glyphicon glyphicon-"+settings.acceptIcon+"' aria-hidden='true'></span> ":"")+settings.acceptText+"</button>";
		modal += "</div>";
		modal += "</div></div></div>";

		this.append( modal );
		$('.tempQModal').on('hidden.bs.modal', function (e) {
			$(this).remove();
		}).modal({ backdrop: 'static', keyboard: false });

		return this;
	}

	$.fn.loadify = function( options ) {
		var settings = $.extend({
			divID: "pleaseWaitDialog",
			txt: false
		}, options );

		var modal = "";
		modal += "<div class='modal fade' id='"+settings.divID+"' data-backdrop='static' data-keyboard='false'><div class='modal-dialog' role='document'><div class='modal-content'>";
		modal += "<div class='modal-header'><h3 style='margin: 0px;'>Cargando...</h3></div><div class='modal-body'>"+((settings.txt)?"<p>"+settings.txt+"</p>":"");
		modal += "<div class='progress'><div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='0' aria-valuemax='100' style='width: 100%'><span class='sr-only'>100% Complete</span></div></div>";
		modal += "</div></div></div></div>";

		this.append( modal );
		$('#'+settings.divID).on('hidden.bs.modal', function (e) {
			$(this).remove();
		});

		return $('#'+settings.divID);
	};
}( jQuery ));

var loadingModal;
loadingModal = loadingModal || (function () {
	var pleaseWaitDiv;
	return {
		showPleaseWait: function( options ) {
			pleaseWaitDiv = $("body").loadify( options ).modal();
		},
		hidePleaseWait: function () {
			pleaseWaitDiv.modal('hide');
		},
	};
})();